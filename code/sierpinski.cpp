#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;


/* =========== FONCTIONS DE DESSIN =========== 
   ============= NE PAS MOFIDIER ============= */
double xprec, yprec, zprec, edge = 600.0; // Taille
double alpha = edge-20;
ofstream imgfile;
const char *imgname = "sierpinski.svg"; // Fichier image

#define TX(X) (alpha*(X)+10)
#define TY(Y) (edge-(alpha*(Y)+30))

void initialise_dessin() {
  imgfile.open(imgname);
  imgfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << (int)edge << "\" height=\"" << (int)edge << "\">";
}
  
void termine_dessin() {
  imgfile << "</svg>";
  imgfile.close();
}

void dessine_triangle(double xA, double yA,double xB, double yB,double xC, double yC) {
  imgfile << "<polygon points=\"" <<  TX(xA) << "," << TY(yA) << " " << TX(xB) << "," << TY(yB) << " " << TX(xC) << "," << TY(yC) << "\" style=\"stroke:black;fill:black;stroke-width:0\" />";
}
/* =================== FIN =================== */

void sierpinski(double xA, double yA,double xB, double yB, double xC, double yC, int n) {
  /* FONCTION A COMPLETER 
     
     Pour A=(xA,yA), B=(xB,yB), C=(xC,yC) et n>=0
     "sierpinski(xA,yA,xB,yB,xC,yC,n)" devra calculer
     l'approximation du triangle de Sierpinski au
     rang n en partant du triangle ABC.

     Afin de realiser le dessin, l'appel a la fonction
     "dessine_triangle(x1,y1,x2,y2,x3,y3)" dessine un triangle
     plein de couleur noire et de sommets les points (x1,y1),
     (x2,y2) et (x3,y3).

     Indication : pensez simple et recursif, 10-15 lignes suffisent...
  */

}

int main() {
  double xA = 0.0, yA = 0.0, xB = 1.0, yB=0.0, xC = 0.5, yC = sqrt(3)/2; // Triangle de depart
  int n = 7;
  initialise_dessin();
  sierpinski(xA,yA,xB,yB,xC,yC,n);
  termine_dessin();
  return 0;
}
