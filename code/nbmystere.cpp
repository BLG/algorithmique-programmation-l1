#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  int nb, k, nbmax, nbprop, parties, victoires;
  char rejouer;
  srand(time(NULL));
  
  nbmax = 1000;
  parties = 0;
  victoires = 0;

  do {
    
    nb = rand()%nbmax+1;
    nbprop = 10;
    k = -1;

    while (nbprop>0 && k!=nb) {
      do {
	cout << "Proposez un nombre (1-" << nbmax << ") [" << nbprop << "]: ";
	cin >> k;
      } while (k<1 || k>nbmax);
      
      if (k>nb) {
	cout << "Plus petit." << endl;
      }
      else if (k<nb) {
	cout << "Plus grand." << endl;
      }

      nbprop = nbprop-1;
    }
    
    if (k==nb) {
      cout << "Gagne !" << endl << endl;
      victoires = victoires + 1;
    }
    else cout << "Perdu, c'etait " << nb << "..." << endl << endl;
    parties = parties + 1;

    do {
      cout << "Rejouer (o=oui, n=non) ? ";
      cin >> rejouer;
    } while (rejouer!='o' && rejouer!='n');
    
  } while (rejouer=='o');

  cout << parties << " partie(s), " << victoires << " victoire(s)" << endl;
    
  return 0;
}
