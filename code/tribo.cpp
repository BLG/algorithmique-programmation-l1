#include <iostream>
using namespace std;

void tribo(int n) {
  int s, u0, u1, u2; // k=2
  u0 = 0;
  u1 = 1;
  u2 = 1;
  for (int i=3; i<=n; ++i) {
    s = u2;
    u2 = u2 + u1 + u0;
    u0 = u1;
    u1 = s;
  }
  cout << u2 << endl;
}

int main() {
  int n;
  cin >> n;
  tribo(n);
  return 0;
}
