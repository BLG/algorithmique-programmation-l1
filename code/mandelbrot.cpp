#include <iostream>
using namespace std;

void mandelbrot(int h, int l, int kmax) {
  double cx, cy, x, y, sx;
  int k;
  for (int i=0; i<h; ++i) {
    for (int j=0; j<l; ++j) {
      cx = -2.0+2.5*(double)j/((double)(l-1));
      cy = 1.0-2.0*(double)i/((double)(h-1));
      x = 0;
      y = 0;
      k = 0;
      while (x*x+y*y<=4.0 && k<kmax) {
	sx = x;
	x = x*x - y*y + cx;
	y = 2*sx*y + cy;
	k = k + 1;
      }
      if (x*x+y*y<=4.0) {
	cout << "##";
      }
      else {
	cout << "  ";
      }
    }
    cout << endl;
  }
} 

int main() {
  mandelbrot(39,50,1000);
  return 0;
}
