#include <iostream>
using namespace std;

void damier(int n) {
  for (int i=1; i<=n; ++i) {
    for (int j=1; j<=n; ++j) {
      if ((i+j)%2==0) {
	cout << "##";
      }
      else {
	cout << "..";
      }
    }
    cout << endl;
  }
}

int main() {
  int n;
  do {
    cout << "n (>=2) ? ";
    cin >> n;
  } while (n<2);
  damier(n);
  return 0;
}
