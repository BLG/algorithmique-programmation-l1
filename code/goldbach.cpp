#include <iostream>
#include <cmath>
using namespace std;

/* La conjecture de Golbach stipule que tout nombre pair n >= 4
   peut s'ecrire comme la somme n = a + b de deux nombres premiers a et b.
*/

bool premier(int n) { // fonction renvoyant true ssi n est premier
  int d;
  bool est_premier;
  est_premier = (n%2!=0);
  d = 3;
  while (est_premier && d<=(int)sqrt(n)) {
    est_premier = (n%d!=0);
    d = d + 2;
  }
  return est_premier; // on renvoie la valeur du booleen est_premier
}

void goldbach_q1() { // affiche toutes les decompositions d'un nombre
  int n, a, b;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<4 || n%2!=0);
  if (n==4) { // cas particulier de 4
    cout << "4 = 2 + 2" << endl;
  }
  else { // autres cas
    for (a=3; a<=n/2; a+=2) { // on parcourt les a impairs
      b = n - a; // on calcule le b correspondant
      if (premier(a) && premier(b)) {
	cout << n << " = " << a << " + " << b << endl;
      }
    }
  }
}

void goldbach_q2() { // affiche tous les nombres decomposables
  int n_max, n, a, b;
  bool est_decomposable;
  cout << "n_max ? ";
  cin >> n_max;
  for (n=4; n<=n_max; n+=2) {
    if (n==4) { // cas particulier de 4
      cout << "4 ";
    }
    else { // autres cas
      est_decomposable = false;
      a = 3;
      while (!est_decomposable && a<=n/2) {
	b = n - a;
	est_decomposable = (premier(a) && premier(b));
	a = a + 2;
      }
      if (est_decomposable) {
	cout << n << " ";
      }
    }
  }
}

int main() {
  cout << "Question 1 :" << endl;
  goldbach_q1();
  
  cout << endl << "Question 2 :" << endl;
  goldbach_q2();
    
  return 0;
}
