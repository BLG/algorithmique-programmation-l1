#include <iostream>
using namespace std;

void carre(int n) {
  for (int i=1; i<=n; ++i) {
    for (int j=1; j<=n; ++j)
      cout << '*';
    cout << endl;
  }
}

void carrevide(int n) {
  for (int i=1; i<=n; ++i) {
    for (int j=1; j<=n; ++j)
      if (i==1 || i==n || j==1 || j==n)
	cout << '*';
      else 
	cout << ' ';
    cout << endl;
  }
}

void triangle(int n) {
  int nblignes;
  nblignes = n/2 + 1;
  for (int i=1; i<=nblignes; ++i) {
    for (int j=1; j<=nblignes-i; ++j)
      cout << ' ';
    for (int j=1; j<=2*i-1; ++j)
      cout << '*';
    cout << endl;
  }
}

void triangle2(int n) {
  int nblignes,nbspc,nbstar;
  nblignes = n/2 + 1;
  nbspc = nblignes-1;
  nbstar = 1;
  for (int i=1; i<=nblignes; ++i) {
    for (int j=1; j<=nbspc; ++j)
      cout << ' ';
    for (int j=1; j<=nbstar; ++j)
      cout << '*';
    cout << endl;
    --nbspc;
    nbstar += 2;
  }
}

int main() {
  int n;
  cin >> n;
  carre(n);
  cout << endl;
  carrevide(n);
  cout << endl;
  triangle(n);
  cout << endl;
  triangle2(n);
  return 0;
}
