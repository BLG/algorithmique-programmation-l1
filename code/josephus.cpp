#include <iostream>
using namespace std;

void josephus(int k, int n) {
  int p = 0;
  for (int i=2; i<=n; ++i)
    p = (p+k)%i;
  cout << p << endl;
}

int main() {
  int k, n;
  cout << "k ? n ?" << endl;
  cin >> k >> n;
  josephus(k,n);
  return 0;
}
