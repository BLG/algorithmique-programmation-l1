#include <iostream>
#include <cmath>
using namespace std;

void exercice1() {
  int a, b, c, d;
  a = 1;
  b = 3;
  b = (b-a)-b;
  c = 3;
  if (b<a) {
    b = 7;
  }
  else {
    if (c>0) {
      c = a;
    }
    else {
      c = b;
    }
  }
  cout << a << "a" << b << "b" << c << "c";
}

void exercice2() { // voile
  int k, i, j;
  cout << "k (impair >=3) ? ";
  cin >> k;
  if (k<3 || k%2==0) {
    cout << "Valeur incorrecte !" << endl;
  }
  else {
    for (i=1; i<=k/2+1; ++i) { // nb de lignes = k/2+1 
      for (j=1; j<=2*i-1; ++j) { // 2i-1 etoiles par ligne
	cout << "*";
      }
      cout << endl;
    }
  }
}

void exercice3() { // perimetre polygone
  double x0, y0, x, y, x_prec, y_prec, dx, dy, p;
  cout << "Premier point : x ? y ? ";
  cin >> x0 >> y0; // on lit le premier point (x0,y0)
  p = 0; // perimetre initialise a 0
  x_prec = x0; // le point precedent (x_prec,y_prec) est initialise a (x0,y0)
  y_prec = y0;
  do { // ici on lit les points suivants :
    cout << "Point suivant : x ? y ? ";
    cin >> x >> y; // on lit le point courant (x,y)
    dx = x - x_prec;
    dy = y - y_prec;
    p = p + sqrt(dx*dx + dy*dy); // on ajoute a p la distance avec le point precedent
    x_prec = x; // le point precedent (x_prec,y_prec) est remplace par le point courant (x,y)
    y_prec = y;
  } while (x!=x0 || y!=y0); // tant que le point courant (x,y) n'est pas (x0,y0)
  cout << "Perimetre = " << p << endl;
}

int main() {
  cout << "Exercice 1 :" << endl;
  exercice1();
  
  cout << endl << endl << "Exercice 2 :" << endl;
  exercice2();
  
  cout << endl << "Exercice 3 :" << endl;
  exercice3();
  
  return 0;
}
