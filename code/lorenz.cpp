#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;


/* =========== FONCTIONS DE DESSIN =========== 
   ============= NE PAS MOFIDIER ============= */
double xprec, yprec, zprec;
double edge = 600.0; // Taille de l'image
double alpha = 6.0; // Zoom
double xb1 = 1.7, yb1 = 1.0, zb1 = 0.0; // Vecteur base 1
double xb2 = -0.333, yb2 = 0.333, zb2 = 0.9; // Vecteur base 2
ofstream imgfile;
const char *imgname = "lorenz.svg"; // Fichier image

#define TX(X) (alpha*(X)+edge/2.0)
#define TY(Y) (edge-(alpha*(Y)+edge/2.0-100.0))
#define P(XA,YA,ZA,XB,YB,ZB) ((XA)*(XB)+(YA)*(YB)+(ZA)*(ZB))
#define P1(X,Y,Z) (P(X,Y,Z,xb1,yb1,zb1))
#define P2(X,Y,Z) (P(X,Y,Z,xb2,yb2,zb2))

void dessine_boite(double x1, double y1, double z1, double x2, double y2, double z2) {
  string s = "fill:white;stroke:gray;stroke-dasharray:5,5";
  imgfile << "<polyline points=\"" << TX(P1(x1,y1,z1)) << "," << TY(P2(x1,y1,z1)) << " " << TX(P1(x2,y1,z1)) << "," << TY(P2(x2,y1,z1)) << " " << TX(P1(x2,y2,z1)) << "," << TY(P2(x2,y2,z1)) << " "  << TX(P1(x1,y2,z1)) << "," << TY(P2(x1,y2,z1)) << " " << TX(P1(x1,y1,z1)) << "," << TY(P2(x1,y1,z1)) << " " << TX(P1(x1,y1,z2)) << "," << TY(P2(x1,y1,z2)) << " " << TX(P1(x2,y1,z2)) << "," << TY(P2(x2,y1,z2)) << " " << TX(P1(x2,y2,z2)) << "," << TY(P2(x2,y2,z2)) << " "  << TX(P1(x1,y2,z2)) << "," << TY(P2(x1,y2,z2)) << " " << TX(P1(x1,y1,z2)) << "," << TY(P2(x1,y1,z2)) << "\" style=\"" << s << "\" />";
  imgfile << "<polyline points=\"" << TX(P1(x2,y1,z1)) << "," << TY(P2(x2,y1,z1)) << " " << TX(P1(x2,y1,z2)) << "," << TY(P2(x2,y1,z2)) << "\" style=\"" << s << "\" />";
  imgfile << "<polyline points=\"" << TX(P1(x2,y2,z1)) << "," << TY(P2(x2,y2,z1)) << " " << TX(P1(x2,y2,z2)) << "," << TY(P2(x2,y2,z2)) << "\" style=\"" << s << "\" />";
  imgfile << "<polyline points=\"" << TX(P1(x1,y2,z1)) << "," << TY(P2(x1,y2,z1)) << " " << TX(P1(x1,y2,z2)) << "," << TY(P2(x1,y2,z2)) << "\" style=\"" << s << "\" />";
}

void initialise_dessin(double x0, double y0, double z0) {
  double norm = sqrt(xb1*xb1+yb1*yb1+zb1*zb1);
  xb1 /= norm; yb1 /= norm; zb1 /= norm;
  norm = sqrt(xb2*xb2+yb2*yb2+zb2*zb2);
  xb2 /= norm; yb2 /= norm; zb2 /= norm;
  imgfile.open(imgname); // Ouverture fichier image
  imgfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << int(edge) << "\" height=\"" << (int)edge << "\">";
  xprec = x0; yprec = y0; zprec = z0;
  dessine_boite(-25.0,-25.0,0.0,25.0,25.0,50.0);
}

void termine_dessin() {
  imgfile << "</svg>";
  imgfile.close(); // Fermeture fichier image
}

void dessine_point(double x, double y, double z) {
  imgfile << "<line x1=\"" << TX(P1(xprec,yprec,zprec)) << "\" y1=\"" << TY(P2(xprec,yprec,zprec)) << "\" x2=\"" << TX(P1(x,y,z)) << "\" y2=\"" << TY(P2(x,y,z)) << "\" style=\"stroke:red\" />";
  xprec = x; yprec = y; zprec = z;
}
/* =================== FIN =================== */


void lorenz(double x0, double y0, double z0, double dt, int tmax) {
  /* FONCTION A COMPLETER 
     
     lorenz(x0,y0,z0,dt,tmax) doit calculer l'evolution du systeme
     depuis les conditions initiales (x0,y0,z0), par pas de temps dt
     et pendant tmax pas de temps.

     A chaque etape de temps, vous afficherez l'etat courant du systeme
     en appelant simplement "dessine_point(x,y,z)". Le programme produira
     alors automatiquement un fichier image "lorenz.svg" que vous
     devrez ouvrir pour regarder le trace obtenu pour votre simulation.
     
     Indication : faites simple, 10-15 lignes suffisent...
  */
  
}

int main() {
  double x0 = 10.0, y0 = 10.0, z0 = 10.0; // Conditions initiales
  double dt = 0.01; // Pas de temps
  int tmax = 5000; // Nombre de points a tracer
  initialise_dessin(x0,y0,z0);
  lorenz(x0,y0,z0,dt,tmax); // Appel a la fonction de simulation
  termine_dessin();
  return 0;
}
