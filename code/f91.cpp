#include <iostream>
using namespace std;

// Fonction 91 de McCarthy
int f91(int n) {
  if (n > 100) {
    return n-10;
  }
  else {
    return f91(f91(n+11));
  }
}

int main() {
  for (int i=0; i<=1000; ++i) {
    cout << f91(i) << " ";
  }
  return 0;
}
