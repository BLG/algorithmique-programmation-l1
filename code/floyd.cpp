#include <iostream>
using namespace std;

int u0 = 52;
int f(int n) {
  if (n>=17) return n-1;
  else return (n+1)%17;
}

void floyd() {
  int tortue, lievre, c, p, m;

  // Etape 1
  tortue = f(u0);
  lievre = f(f(u0));
  while (lievre != tortue) {
    tortue = f(tortue);
    lievre = f(f(lievre));
  }

  // Etape 2 - Period. min.
  c = 1;
  lievre = f(lievre);
  while (lievre != tortue) {
    lievre = f(lievre);
    ++c;
  }
  p = c;
  
  /* Etape 3
     La tortue part ici d'un multiple de la période, le lièvre de 0,
     et ils maintiennent leur écart constant donc dès que le lièvre
     entre dans la boucle, ils prennent la même valeur.
  */
  c = 0;
  lievre = u0;
  while (lievre != tortue) {
    tortue = f(tortue);
    lievre = f(lievre);
    ++c;
  }
  m = c;

  cout << m << ' ' << p << endl;
}

int main() {
  floyd();
  return 0;
}
