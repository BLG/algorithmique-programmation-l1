#include <iostream>
#include <cstdio>
#include <ctime>
using namespace std;

// Exercice 1
void remplir_alea(int *t, int taille, int min, int max) {
  for (int i=0; i<taille; ++i) {
    t[i] = rand()%(max-min+1)+min;
  }
}

void afficher(int *t, int taille) {
  cout << "[ ";
  for (int i=0; i<taille; ++i) {
    cout << t[i] << " ";
  }
  cout << "]" << endl;
}

int maximum(int *t, int taille) {
  int m = t[0];
  for (int i=1; i<taille; ++i) {
    if (t[i]>m) {m = t[i];}
  }
  return m;
}

int posmax(int *t, int taille) {
  int p = 0;
  for (int i=1; i<taille; ++i) {
    if (t[i]>t[p]) {p = i;}
  }
  return p;
}

int posmin(int *t, int taille) {
  int p = 0;
  for (int i=1; i<taille; ++i) {
    if (t[i]<t[p]) {p = i;}
  }
  return p;
}

int minimum(int *t, int taille) {
  return t[posmin(t,taille)];
}

double moyenne(int *t, int taille) {
  double m = 0;
  for (int i=0; i<taille; ++i) {
    m = m + (double)t[i];
  }
  m = m/(double)taille;
  return m;
}

int presence(int *t, int taille, int e) {
  for (int i=0; i<taille; ++i) {
    if (t[i]==e) {return i;}
  }
  return -1;
}

void echange(int *t, int i, int j) {
  int s = t[i];
  t[i] = t[j];
  t[j] = s;
}

void miroir(int *t, int taille) {
  for (int i=0; i<taille/2; ++i) {
    echange(t,i,taille-1-i);
  }
}

void tri_selection(int *t, int taille) {
  int p;
  for (int fin=taille-1; fin>0; --fin) {
    p = posmax(t,fin+1);
    echange(t,p,fin);
  }
}

void tri_bulle(int *t, int taille) {
  for (int i=0; i<taille-1; ++i) {
    for (int j=0; j<taille-1-i; ++j) {
      if (t[j]>t[j+1]) {echange(t,j,j+1);}
    }
  }
}

int recherche_dicho(int *t, int taille, int e) {
  int debut, milieu, fin;
  debut = 0;
  fin = taille - 1;
  while (debut != fin) {
    milieu = (debut+fin)/2;
    if (e<t[milieu]) {fin = milieu-1;}
    else if (e>t[milieu]) {debut = milieu+1;}
    else {return milieu;}
  }
  if (t[debut]==e) {return debut;}
  else {return -1;}
}


// Main
int main() {
  int e, taille = 10;
  int t[taille];
  srand(time(NULL));
  remplir_alea(t,taille,10,50);
  e = t[0];
  afficher(t,taille);
  miroir(t,taille);
  afficher(t,taille);
  //tri_bulle(t, taille);
  tri_selection(t, taille);
  afficher(t,taille);
  cout << "Position de " << e << " : " << recherche_dicho(t,taille,e) << endl;
  return 0;
}
