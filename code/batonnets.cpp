#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  int nb, k; // nb pour le nombre de batonnets, k pour ceux pris a chaque tour
  char joueur; // joueur contiendra le joueur en cours
  srand(time(NULL)); // initialisation du generateur aleatoire
  
  do { // on demande le nombre initial de batonnets
    cout << "Nombre de batonnets (>=4) ? ";
    cin >> nb;
  } while (nb < 4);
  
  do { // on demande le joueur qui commence
    cout << "Qui commence (j=joueur, t=tigre) ? ";
    cin >> joueur;
  } while (joueur!='j' && joueur!='t');
  
  while (nb > 0) { // tant qu'il reste des batonnets, on joue
    for (int i=0; i<nb; ++i) cout << "|"; // on affiche les batonnets
    cout << "  (" << nb << ")" << endl; // et leur nombre
      
    if (joueur=='j') { // si c'est au candidat de jouer
      do { // on lui demande son coup
	cout << "A vous de jouer (1-"<< min(3,nb) << ") : ";
	cin >> k;
      } while (k<1 || k>3 || k>nb);
      nb = nb - k; // on met a jour le nombre de batonnets
      joueur = 't'; // on change de joueur
    }
    else { // sinon, le tigre joue
      if (nb==1) k = 1; // cas particulier : il reste 1 batonnet
      else if (nb%4 == 1) k = rand()%3+1; // coup aleatoire
      else if (nb%4 == 0) k = 3; // cf. strategie gagnante
      else if (nb%4 == 2) k = 1; // cf. strategie gagnante
      else k = 2; // cf. strategie gagnante
      cout << "Le tigre prend " << k << " batonnet(s)." << endl; // on affiche le coup
      nb = nb - k; // on met a jour le nombre de batonnets
      joueur = 'j'; // on change de joueur
    }
  }
  
  // Fin de partie
  if (joueur=='j') cout << "Bravo ! Vous avez gagne !" << endl;
  else cout << "Grrrr ! Le tigre a gagne !" << endl;

  return 0;
}
