#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

// Exercice 1
int maximum(int x, int y) {
  if (x<=y) return y;
  else return x;
}

int maximum3(int x, int y, int z) {
  return maximum(x, maximum(y,z));
}

void exo1_q3() {
  int n, M;
  M = 0;
  do {
    cout << "n (0 pour arreter) ? ";
    cin >> n;
    M = maximum(M,n);
  } while (n!=0);
  cout << "Maximum : " << M << endl;
}


// Exercice 2
int occurrences(int n, int c) {
  int cpt = 0;
  while (n!=0) {
    if (n%10 == c) {cpt = cpt + 1;}
    n = n/10;
  }
  return cpt;
}

void exo2_q2() {
  int n, c;
  cout << "n ? ";
  cin >> n;
  c = rand()%10;
  cout << "Nb de " << c << " dans " << n << " : " << occurrences(n,c) << endl;
}


// Exercice 3
int lire() {
  int n;
  cout << "entier ? ";
  cin >> n;
  return n;
}

void affiche(int n) {
  cout << n << endl;
}

int produit(int x, int y) {
  return x*y;
}

void produit5() {
  int a1,a2,a3,a4,a5,p;
  cout << "a1 ? ";
  cin >> a1;
  cout << "a2 ? ";
  cin >> a2;
  cout << "a3 ? ";
  cin >> a3;
  cout << "a4 ? ";
  cin >> a4;
  cout << "a5 ? ";
  cin >> a5;
  p = a1*a2*a3*a4*a5;
  cout << a1 << "*" << a2 << "*" << a3 << "*" << a4 << "*" << a5 << " = " << p << endl;
}


// Exercice 4
int fact(int n) {
  int f = 1;
  for (int i=2; i<=n; ++i) {
    f = f*i;
  }
  return f;
}

int binom(int n, int p) {
  return fact(n)/(fact(p)*fact(n-p));
}

void triangle_pascal(int k) {
  for (int n=0; n<k; ++n) {
    for (int p=0; p<=n; ++p)
      cout << binom(n,p) << "\t";
    cout << endl;
  }
}


// Exercice 5
int longueur(int n) {
  int l = 0;
  while (n!=0) {
    l = l + 1;
    n = n/10;
  }
  return l;
}

void exo5_q2(int m) {
  int a, l;
  l = 0;
  for (int i=1; i<=m; ++i) {
    cout << "entier " << i  << "/" << m << " ? ";
    cin >> a;
    l = maximum(l,longueur(a));
  }
  cout << "Longueur max : " << l << endl;
}


// Exercice 6
int maxn(int n) {
  int c;
  c = 0;
  while (n!=0) {
    c = maximum(c,n%10);
    n = n/10;
  }
  return c;
}

void exo6_q2(int m) {
  int a, c;
  c = -1;
  for (int i=1; i<=m; ++i) {
    cout << "entier " << i  << "/" << m << " ? ";
    cin >> a;
    c = maximum(c,maxn(a));
  }
  cout << "Chiffre max : " << c << endl;
}


// Exercice 7
void chiffre_en_lettres(int c) {
  string chiffres[10] = {"zero","un","deux","trois","quatre","cinq","six","sept","huit","neuf"};
  cout << chiffres[c%10];
}

int chiffre_gauche(int n) {
  int a, m, c;
  m = n;
  a = 1;
  while (m!=0) {
    c = m%10;
    m = m/10;
    a = a*10;
  }
  chiffre_en_lettres(c);
  return n - c*a/10;
}

void nombre_en_lettres() {
  int n;
  cout << "n ? ";
  cin >> n;
  while (n!=0) {
    n = chiffre_gauche(n);
    cout << " ";
  }
  cout << endl;
}


// Exercice 8
int de() {
  return rand()%6+1;
}

void tri3(int &x, int &y, int &z) {
  int a = x, b = y, c = z;
  x = min(a,min(b,c));
  z = max(a,max(b,c));
  y = a+b+c - x - z;
}

int jeu(int mise) {
  int gain, x = de(), y = de(), z = de();
  cout << "Mise : " << mise << endl;
  cout << "Des : " << x << " " << y << " " << z << endl;
  // On trie pour avoir x <= y <= z
  tri3(x,y,z);
  if (x==1 && y==2 && z==4) gain = 100*mise;
  else if (x==y && x==z) {
    if (x==6) gain = 10*mise;
    else gain = 5*mise;
  }
  else if (x==y || y==z) gain = mise;
  else gain = 0;
  cout << "Gain : " << gain << endl;
  return gain-mise;
}

void partie(int argent) {
  bool rejouer = true;
  char c;
  int mise;
  while (rejouer && argent>0) {
    cout << "Vous avez " << argent << " euros." << endl;
    do {
      cout << "Mise (1-" << min(10,argent) << ") ? ";
      cin >> mise;
    } while (mise<1 || mise>argent || mise>10);
    argent = argent + jeu(mise);
    if (argent>0) {
      cout << "Rejouer (o=oui, reste=non) ? ";
      cin >> c;
      rejouer = (c=='o');
    }
  }
  cout << "Vous partez avec " << argent << " euros." << endl;
}


// MAIN
int main() {
  srand(time(NULL));
  //exo1_q3();
  //exo2_q2();
  //produit5();
  //triangle_pascal(8);
  //exo5_q2(5);
  //exo6_q2(5);
  //nombre_en_lettres();
  partie(10);
  return 0;
}
