#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

double pi_montecarlo(int n) {
  double x, y, p;
  int cpt = 0;
  for (int i=1; i<=n; ++i) {
    x = ((double)rand()/(double)RAND_MAX)*2.0-1.0;
    y = ((double)rand()/(double)RAND_MAX)*2.0-1.0;
    if (x*x+y*y<=1) {
      cpt = cpt + 1;
    }
  }
  p = 4.0*(double)cpt/(double)n;
  return p; 
}

int main() {
  //srand(42);
  srand(time(NULL));
  cout << pi_montecarlo(5000000) << endl;
  return 0;
}
