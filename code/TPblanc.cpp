#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void verite() {
  int i, j, k;
  bool x, y, z;
  for (i=0; i<=1; ++i) {
    x = (i==1);
    for (j=0; j<=1; ++j) {
      y = (j==1);
      for (k=0; k<=1; ++k) {
	z = (k==1);
	if ((x && y) || (!z)) {
	  cout <<i << " " << j << " " << k << " 1" << endl;
	}
	else {
	  cout << i << " " << j << " " << k << " 0" << endl;
	}
      }
    }
  }
}

void damier(int n) {
  for (int i=1; i<=n; ++i) {
    for (int j=1; j<=n; ++j) {
      if ((i+j)%2==0) {
	cout << "##";
      }
      else {
	cout << "..";
      }
    }
    cout << endl;
  }
}

int binom(int n, int k) {
  int binom;
  binom = 1;
  for (int i=1; i<=k; ++i) {
    binom = (n-k+i)*binom/i;
  }
  return binom;
}

void triangle(int n) {
  for (int i=0; i<=n; ++i) {
    for (int j=0; j<=i; ++j) {
      if (binom(i,j)%2==1) {
	cout << "@@";
      }
      else {
	cout << "  ";
      }
    }
    cout << endl;
  }
}

void pi_montecarlo(int n) {
  double x, y, p;
  int cpt = 0;
  for (int i=1; i<=n; ++i) {
    x = ((double)rand()/(double)RAND_MAX)*2.0-1.0;
    y = ((double)rand()/(double)RAND_MAX)*2.0-1.0;
    if (x*x+y*y<=1) {
      cpt = cpt + 1;
    }
  }
  p = 4.0*(double)cpt/(double)n;
  cout << p << endl; 
}

void mandelbrot(int h, int l, int kmax) {
  double cx, cy, x, y, sx;
  int k;
  for (int i=0; i<h; ++i) {
    for (int j=0; j<l; ++j) {
      cx = -2.0+2.5*(double)j/((double)(l-1));
      cy = 1.0-2.0*(double)i/((double)(h-1));
      x = 0;
      y = 0;
      k = 0;
      while (x*x+y*y<=2.0 && k<kmax) {
	sx = x;
	x = x*x - y*y + cx;
	y = 2*sx*y + cy;
	k = k + 1;
      }
      if (x*x+y*y<=2.0) {
	cout << "##";
      }
      else {
	cout << "  ";
      }
    }
    cout << endl;
  }
} 

int main() {
  //  srand(42);
  srand(time(NULL));
  verite();
  cout << endl;
  damier(10);
  cout << endl;
  triangle(15);
  cout << endl;
  pi_montecarlo(1000000);
  cout << endl;
  mandelbrot(39,50,2000);
  return 0;
}
