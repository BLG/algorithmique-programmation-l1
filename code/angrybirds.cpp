#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

int main() {
  int coups, pos1, pos2, pos3, h1, h2, h3, x, y;
  double vitesse, angle, angle_rad, v0x, v0y;
  bool ennemi1_vivant, ennemi2_vivant, ennemi3_vivant, rejouer;
  char c;
  
  // Parametres fixes
  double pi = 3.14;
  double g = 9.81;
  int nbcoups = 7;
  int hauteur = 20;
  int largeur = 60;
  double vmin = 1.0;
  double vmax = 50.0;

  // Initialisation globale
  srand(time(NULL));
  int jouees = 0;
  int gagnees = 0;
  
  do {
    // Initialisation de partie
    coups = nbcoups;
    ennemi1_vivant = true;
    ennemi2_vivant = true;
    ennemi3_vivant = true;
    pos1 = rand()%10+30;
    h1 = rand()%5+3;
    pos2 = rand()%10+40;
    h2 = rand()%10+5;
    pos3 = rand()%10+50;
    h3 = rand()%5+3;
    
    // Partie
    while (coups>0 && (ennemi1_vivant || ennemi2_vivant || ennemi3_vivant)) {
      // Affichage
      for (int i=0; i<=hauteur+1; ++i) {
	for (int j=0; j<=largeur+1; ++j) {
	  if (i==0 || i==hauteur+1 || j==0 || j==largeur+1) {
	    cout << "#";
	  }
	  else if (ennemi1_vivant && j==pos1) {
	    if (i==hauteur-h1) cout << "@";
	    else if (i>hauteur-h1) cout << "X";
	    else cout << " ";
	  }
	  else if (ennemi2_vivant && j==pos2) {
	    if (i==hauteur-h2) cout << "@";
	    else if (i>hauteur-h2) cout << "X";
	    else cout << " ";
	  }
	  else if (ennemi3_vivant && j==pos3) {
	    if (i==hauteur-h3) cout << "@";
	    else if (i>hauteur-h3) cout << "X";
	    else cout << " ";
	  }	
	  else cout << " ";
	}
	cout << endl;
      }
      
      // Tir
      do {
	cout << "Angle (0-90) ? ";
	cin >> angle;
      } while (angle<0 || angle>90);
      do {
	cout << "Vitesse (" << vmin << "-" << vmax << ") ? ";
	cin >> vitesse;
      } while (vitesse<vmin || vitesse>vmax);
      angle_rad = pi*angle/180.0;
      v0x = vitesse*cos(angle_rad);
      v0y = vitesse*sin(angle_rad);
      
      // Affichage tir
      for (int i=0; i<=hauteur+1; ++i) {
	y = hauteur-i;
	for (int j=0; j<=largeur+1; ++j) {
	  x = j-1;
	  if (i==0 || i==hauteur+1 || j==0 || j==largeur+1) {
	    cout << "#";
	  }
	  else if (y==round(-0.5*g*x*x/(v0x*v0x)+v0y*x/v0x)) {
	    cout << "*";
	  } 
	  else if (ennemi1_vivant && j==pos1) {
	    if (i==hauteur-h1) cout << "@";
	    else if (i>hauteur-h1) cout << "X";
	    else cout << " ";
	  }
	  else if (ennemi2_vivant && j==pos2) {
	    if (i==hauteur-h2) cout << "@";
	    else if (i>hauteur-h2) cout << "X";
	    else cout << " ";
	  }
	  else if (ennemi3_vivant && j==pos3) {
	    if (i==hauteur-h3) cout << "@";
	    else if (i>hauteur-h3) cout << "X";
	    else cout << " ";
	  }	
	  else cout << " ";
	}
	cout << endl;
      }

      // Bilan des degats
      x = pos1 - 1;
      if (ennemi1_vivant && h1==round(-0.5*g*x*x/(v0x*v0x)+v0y*x/v0x)) {
	cout << "Ennemi 1 touche !" << endl;
	ennemi1_vivant = false;
      }
      x = pos2 - 1;
      if (ennemi2_vivant && h2==round(-0.5*g*x*x/(v0x*v0x)+v0y*x/v0x)) {
	cout << "Ennemi 2 touche !" << endl;
	ennemi2_vivant = false;
      }
      x = pos3 - 1;
      if (ennemi3_vivant && h3==round(-0.5*g*x*x/(v0x*v0x)+v0y*x/v0x)) {
	cout << "Ennemi 3 touche !" << endl;
	ennemi3_vivant = false;
      }
      
      coups = coups - 1;
      cout << "Il reste " << coups << " oiseau(x)." << endl;
    }

    // Fin de partie
    if (ennemi1_vivant || ennemi2_vivant || ennemi3_vivant) {
      cout << "Perdu !" << endl;
    }
    else {
      cout << "Bravo, partie gagnee !" << endl;
      gagnees = gagnees + 1;
    }
    jouees = jouees + 1;
    
    // Rejouer ou pas
    cout << "Rejouer (o=oui, reste=non) ? ";
    cin >> c;
    rejouer = (c=='o');
  } while (rejouer);

  // Affichage du bilan
  cout << "Bilan : " << gagnees << "/" << jouees << " parties gagnees." << endl;

  return 0;
}
