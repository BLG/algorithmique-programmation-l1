#include <iostream>
#include <cmath>
using namespace std;

/* Newton : x(n+1) = x(n) - f(x(n))/f'(x(n)) 
   Pour calculer sqrt(a), f(x) = x^2 - a
*/

double mysqrt(double a, int p) {
  double x = a/2;
  for (int i=1; i<=p; ++i)
    x = x - (x*x-a)/(2*x);
  return x;
}

void test_sqrt(int n, int p) {
  double myr, r;
  for (int i=0; i<n; ++i) {
    myr = mysqrt((double)i,p);
    r = sqrt((double)i);
    cout << "sqrt(" << i << ")\t" << myr << "\t\t" << r << "\t" << (myr==r ? "" : "<>") << endl;
  }
}

double fact(int n) { // approx fact double
  double f = 1.0;
  for (int i=2; i<=n; ++i) {f = f*(double)i;}
  return f;
}

double mycos(double x, int p) {
  double c = 1.0;
  for (int i=1; i<=p; ++i) {
    if (i%2==0) {c = c + pow(x,2*i)/((double)fact(2*i));}
    else {c = c - pow(x,2*i)/((double)fact(2*i));}
  }
  return c;
}

void test_cos(int n, int p) {
  double myr, r, v;
  for (int i=0; i<n; ++i) {
    v = i*3.14/(double)(n-1);
    myr = mycos(v,p);
    r = cos(v);
    cout << "cos(" << v << ")\t" << myr << "\t" << r << "\t" << (myr==r ? "" : "<>") << endl;
  }
}

bool int_div_3(int n) {
  int s = 0;
  while (n!=0) {
    s = s + n%10;
    n = n/10;
  }
  return (s%3==0);
}

bool grand_div_3(const char *t, int taille) {
  int s = 0;
  for (int i=0; i<taille; ++i) {
    s = s + ((int)t[i]-(int)'0');
  }
  return (s%3==0);
}

int main() {
  test_sqrt(200,30);
  
  cout << fact(12) << endl; // 12 max pour fact en int
  cout << fact(13) << endl;
  test_cos(50,30); // 6 max pour p avec fact int
  
  string s = "123454984844686464";
  cout << s << " " << (grand_div_3(s.c_str(),s.length()) ? "OK" : "KO") << endl;
  s = "5476113456184354";
  cout << s << " " << (grand_div_3(s.c_str(),s.length()) ? "OK" : "KO") << endl;
  
  return 0;
}
