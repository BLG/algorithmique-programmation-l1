#include <iostream>
#include <fstream>
using namespace std;

ifstream txtfile;

// Cryptage
char min2maj(char c) { // convertit une minuscule en majuscule
  return (char)((int)c-(int)'a'+(int)'A');
}

void cryptage(char *substitution) {
  char c;
  txtfile >> noskipws;
  while (txtfile >> c) {
    if ((int)'a'<=(int)c && (int)c<=(int)'z') {
      cout << substitution[(int)c-(int)'a'];
    }
    else if ((int)'A'<=(int)c && (int)c<=(int)'Z') {
      cout << min2maj(substitution[c-(int)'A']);
    }
    else {
      cout << c;
    }
  }
}

void inverse_sub(char *sub_src, char *sub_dest) {
  // inverse une substitution
  for (int i=0; i<26; ++i) {
    sub_dest[sub_src[i]-(int)'a'] = i;
  }
}

void decryptage(char *sub) {
  char invsub[26];
  inverse_sub(sub, invsub);
  cryptage(invsub);
}


// Analyse frequentielle
void analyse(int *occur, int &total) {
  char c;
  txtfile >> noskipws;
  total = 0;
  for (int i=0; i<26; ++i) {
    occur[i] = 0;
  }
  while (txtfile >> c) {
    if ((int)'a'<=(int)c && (int)c<=(int)'z') {
      ++(occur[(int)c-(int)'a']);
      ++total;
    }
    else if ((int)'A'<=(int)c && (int)c<=(int)'Z') {
      ++(occur[(int)c-(int)'A']);
      ++total;
    }
  }
}

void affiche_analyse(int *occur, int total) {
  for (int i=0; i<26; ++i)
    cout << (char)(i+(int)'a') << "  " << 100.0*(double)occur[i]/(double)total << " %" << endl;
}

// Tri bulle
void echange(int &x, int &y) {int s = x; x = y; y = s;}
void echange(char &x, char &y) {char s = x; x = y; y = s;}

void tri(int *t, char *reference, char *sub) {
  int lettres[26];
  for (int i=0; i<26; ++i) lettres[i] = i;
  for (int i=0; i<25; ++i)
    for (int j=0; j<25-i; ++j)
      if (t[j]>t[j+1]) {
	echange(t[j],t[j+1]);
	echange(lettres[j],lettres[j+1]);
      }
  for (int i=0; i<26; ++i) sub[lettres[i]] = reference[i];
}


// Main
int main(int argc, char **argv) {
  int occur[26];
  int total;
  char rot13[26]={'n','o','p','q','r','s','t','u','v','w','x','y','z','a','b','c','d','e','f','g','h','i','j','k','l','m'};
  char ref_fr[26]={'w','k','y','x','z','h','j','f','b','g','q','v','c','p','m','d','o','l','u','r','n','t','s','i','a','e'};
  char sub[26];
  
  if (argc > 0 && (string)argv[1]=="decrypt") {
    txtfile.open("texte2.txt");
    analyse(occur, total);
    affiche_analyse(occur, total);
    tri(occur, ref_fr, sub);
    txtfile.close();
    
    txtfile.open("texte2.txt");
    cryptage(sub);
    txtfile.close();    
  }
  else {
    txtfile.open("texte.txt");
    cryptage(rot13);
    txtfile.close();
  }
    
  return 0;
}
