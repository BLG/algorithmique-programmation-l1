#include <iostream>
using namespace std;

#define NMAX 100

// Q1
int saut(int pos, int *t, int taille) {
  int nouv_pos;
  if (pos<0 || pos>=taille) return -1;
  else {
    nouv_pos = pos + t[pos];
    if (nouv_pos<0 || nouv_pos>=taille) return -1;
    else return nouv_pos;
  }
}


// Q2
void acceleration(int *t, int taille, int *t2) {
  int i1,i2;
  for (int i0=0; i0<taille; ++i0) {
    i1 = saut(i0,t,taille);
    if (i1>=0) {
      i2 = saut(i1,t,taille);
      if (i2>=0) t2[i0] = i2-i0;
      else t2[i0] = taille;
    }
    else t2[i0] = taille;
  }
}

void copie(int *tsrc, int taille, int *tdst) {
  for (int i=0; i<taille; ++i) 
    tdst[i] = tsrc[i];
}

void positions_libres(int *t, int taille) {
  int t2[NMAX];
  int n = taille;
  while (n!=0) {
    acceleration(t,taille,t2);
    copie(t2,taille,t); // pas optimal mais contraintes...
    n = n/2;
  }
  for (int i=0; i<taille; ++i)
    if (t2[i]>=taille) cout << i << " est une position libre." << endl;
}


/* Q3, independante des precedentes
   On suppose le tableau totalement bouclant :
   toute position est dans un cycle
*/
int pgcd(int a, int b) {
  int r = a%b;
  while (r!=0) {
    a = b;
    b = r;
    r = a%b;
  }
  return b;
}

int ppcm(int a, int b) {
  return a*b/pgcd(a,b);
}

int taille_cycle(int p0, int *t, int taille) {
  int p, l;
  l = 1;
  p = p0+t[p0];
  while (p!=p0) {
    p = p+t[p];
    l = l+1;
  }
  return l;
}

int temps_retour(int *t, int taille) {
  int r = 1;
  for (int i=0; i<taille; ++i) 
    r = ppcm(r,taille_cycle(i,t,taille));
  return r;
}


// MAIN
int main() {
  int t[10] = {2,2,2,2,2,2,2,2,2,-2};
  positions_libres(t,10);
  int t2[10] = {2,2,-2,2,2,-4,1,1,1,-5};
  cout << temps_retour(t2,10) << endl;
  return 0;
}
