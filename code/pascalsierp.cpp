#include <iostream>
using namespace std;

int binom(int n, int k) {
  int binom;
  binom = 1;
  for (int i=1; i<=k; ++i) {
    binom = (n-k+i)*binom/i;
  }
  return binom;
}

void triangle(int n) {
  for (int i=0; i<=n; ++i) {
    for (int j=0; j<=i; ++j) {
      if (binom(i,j)%2==1) {
	cout << "@@";
      }
      else {
	cout << "  ";
      }
    }
    cout << endl;
  }
}

int main() {
  triangle(15);
  return 0;
}
