#include <iostream>
#include <cmath>
using namespace std;

void exercice1() { // somme
  int n, S, i;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<0);
  S = 0;
  for (i=1; i<=n; ++i) {
    S = S + i;
  }
  cout << "Somme : " << S << endl;
}

void exercice2() { // exponentiation
  int x, n, i;
  double res;
  cout << "x ? n ? ";
  cin >> x >> n;
  res = 1;
  for (i=1; i<=abs(n); ++i) {
    res = res * (double)x;
  }
  if (n<0) res = 1/res;
  cout << x << "^" << n << " = " << res << endl;
}

void exercice2_bis() { /* exponentiation rapide
			  basee sur la formule 
			  x^n = (x^2)^(n/2) si n est pair
			        x * (x^2)^((n-1)/2) si n est impair */
  int x, y, n, k;
  double res;
  cout << "x ? n ? ";
  cin >> x >> n;
  y = x;
  res = 1.0;
  k = abs(n);
  while (k>0) {
    if (k%2!=0) {
      res = res * (double)y;
    }
    y = y*y;
    k = k/2;
  }
  if (n<0) {
    res = 1/res;
  }
  cout << x << "^" << n << " = " << res << endl;
}

void exercice3() { // couples
  int p, q, i, j;
  do {
    cout << "p ? q ? ";
    cin >> p >> q;
  } while (p<0 || q<0);
  for (i=1; i<=p; ++i) {
    for (j=1; j<=q; ++j) {
      cout << "(" << i << "," << j << ") ";
    }
  }
}

void exercice4() { // triplets
  int n, x, y, z;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<1);
  for (x=1; x<=n-2; ++x) {
    for (y=x+1; y<=n-1; ++y) {
      for (z=y+1; z<=n; ++z) {
	cout << "{" << x << "," << y << "," << z << "} ";
      }
    }
  }
}

void exercice5_q1() { // factorielle
  int n, f, i;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<0);
  f = 1;
  for (i=2; i<=n; ++i) {
    f = f*i;
  }
  cout << n << "! = " << f << endl;
}

void exercice5_q2() { /* coefficient binomial
			 solution basee sur la formule :
			 binom(n,k) = n/k * binom(n-1,k-1) */
  int n, k, binom, i;
  do {
    cout << "n ? k ? ";
    cin >> n >> k;
  } while (k<0 || n<k);
  binom = 1;
  for (i=1; i<=k; ++i) {
    binom = (n-k+i)*binom/i;
  }
  cout << "binom(" << n << "," << k << ") = " << binom << endl;
}

void exercice6() { // min et max de liste
  int x, m, M;
  cout << "Entrer un nombre (negatif pour arreter) : ";
  cin >> x;
  if (x<0) {
    cout << "Liste vide !" << endl;
  }
  else {
    m = x;
    M = x;
    while (x>=0) {
      if (x<m) {
	m = x;
      }
      else if (x>M) {
	M = x;
      }
      cout << "Entrer un nombre (negatif pour arreter) : ";
      cin >> x;
    }
    cout << "min = " << m << ", max = " << M << endl;
  }
}

void exercice7() { // miroir
  int n, m;
  cout << "n ? ";
  cin >> n;
  m = 0;
  while (n!=0) {
    m = 10*m + n%10;
    n = n/10;
  }
  cout << m << endl;
}

void exercice8_q1() { // rectangle plein
  int h, l, i, j;
  cout << "hauteur ? largeur ? ";
  cin >> h >> l;
  for (i=1; i<=h; ++i) {
    for (j=1; j<=l; ++j) {
      cout << "*";
    }
    cout << endl;
  }
}

void exercice8_q2() { // rectangle vide
  int h, l, i, j;
  cout << "hauteur ? largeur ? ";
  cin >> h >> l;
  for (i=1; i<=h; ++i) {
    for (j=1; j<=l; ++j) {
      if (i==1 || i==h || j==1 || j==l) {
	cout << "*";
      }
      else {
	cout << " ";
      }
    }
    cout << endl;
  }
}

void exercice9() { // table multiplication
  int n, i, j;
  cout << "Taille de la table ? ";
  cin >> n;
  for (i=1; i<=n; ++i) {
    for (j=1; j<=n; ++j) {
      cout << i*j << "\t"; // tabulation pour alignement
    }
    cout << endl;
  }
}

void exercice10() { // nb parfaits
  int n, n_max, d, S;
  cout << "n_max ? ";
  cin >> n_max;
  for (n=1; n<=n_max; ++n) {
    S = 0;
    for (d=1; d<n; ++d) {
      if (n%d==0) {
	S = S + d;
      }
    }
    if (S==n) {
      cout << n << " est parfait." << endl;
    }
  }
}
/* N.B. Les entiers representes par le type int sont confines dans
   l'intervalle [-2^31, 2^31-1] et donc dans [0, 2^31-1] pour les positifs.
   Il n'y a que 5 nombres parfaits dans cet intervalle ! */


void exercice11() { // nb premiers
  int n, d;
  bool est_premier;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<2);
  est_premier = (n%2!=0);
  d = 3;
  while (est_premier && d<=(int)sqrt(n)) {
    est_premier = (n%d!=0);
    d = d + 2;
  }
  if (est_premier) {
    cout << n << " est premier." << endl;
  }
  else {
    cout << n << " n'est pas premier." << endl;
  }
}

void exercice13() {
  int j, h;
  string jours[7] = {"Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"};
  do {
    cout << "Jour (1-7) ? Heure (0-23) ? ";
    cin >> j >> h;
  } while (j<1 || j>7 || h<0 || h>23);
  cout << jours[j-1] << ", " << (j-1)*24+h << "h depuis le debut de la semaine." << endl;
}

void exercice14() { // PGCD (Euclide)
  int sa, sb, a, b, r;
  do {
    cout << "a ? b ? ";
    cin >> a >> b;
  } while (b>a || b<0);
  sa = a;
  sb = b;
  r = a%b;
  while (r!=0) {
    a = b;
    b = r;
    r = a%b;
  }
  cout << "PGCD(" << sa << "," << sb << ") = " << b << endl;
}

void exercice15() { // Fibonacci
  int n, u0, un, s, i;
  do {
    cout << "n ? ";
    cin >> n;
  } while (n<0);
  u0 = 1;
  un = 1;
  for (i=2; i<=n; ++i) {
    s = un;
    un = un + u0;
    u0 = s;
  }
  cout << "u_" << n << " = " << un << endl;
}

void exercice16() { // nb pythagoriciens
  int n_max, n, a;
  double b;
  bool est_pythagoricien;
  cout << "n_max ? ";
  cin >> n_max;
  for (n=1; n<=n_max; ++n) {
    est_pythagoricien = false;
    a = 1;
    while (!est_pythagoricien && a<=(int)((double)n/sqrt(2))) {
      b = sqrt(n*n - a*a); // calcule b tel que n^2 = a^2 + b^2
      est_pythagoricien = (b==(double)((int)b)); // teste si b est entier
      a = a + 1;
    }
    if (est_pythagoricien) {
      cout << n << " est pythagoricien : " << n << "^2 = " << a-1 << "^2 + " << b << "^2" << endl;
    }
  }
}
/* N.B. Les entiers pythagoriciens sont exactement les longueurs possibles de 
   l'hypothenuse d'un triangle rectangle dont tous les cotes sont entiers.
   Pour culture, la solution generale de l'equation x^2 + y^2 = z^2 avec
   x,y,z > 0, PGCD(x,y) = 1 et x pair est de la forme
   x = 2ab, y = a^2 - b^2, z = a^2 + b^2 ou
   a > b > 0, a et b ne sont pas de meme parite et PGCD(a,b) = 1 */


void exercice18() { // moyenne
  int n, a, c1, c2, c3, i;
  double m;
  cout << "n ? ";
  cin >> n;
  if (n<=0) {
    cout << -1 << endl;
  }
  else {
    c1 = 0;
    c2 = 0;
    c3 = 0;
    for (i=1; i<=n; ++i) {
      do {
	cout << "a_" << i << " ? ";
	cin >> a;
      } while (a<1 || a>3);
      if (a==1) {
	c1 = c1 + 1;
      }
      else if (a==2) {
	c2 = c2 + 1;
      }
      else {
	c3 = c3 + 1;
      }
    }
    m = ((double)(c1+c2*2+c3*3))/(double)n;
    cout << "Moyenne = " << m << endl;
  }
}

int main() {
  // Modifier cette ligne pour compiler l'exercice desire :
  exercice2_bis();
  
  return 0;
}
