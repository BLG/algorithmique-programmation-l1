\documentclass[11pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tgpagella}

\usepackage{geometry}
\geometry{lmargin=2cm, rmargin=2cm, vmargin=2.5cm} %%% Marges

\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}

\usepackage{amsthm}
\newtheorem{remark}{Remarque}
\newtheorem{example}{Exemple}

\usepackage{listings}
\lstset{language=C++}

\usepackage{xspace}
\usepackage{color}
%\usepackage{stmaryrd}
\usepackage{url}
\usepackage{scrtime}

\author{BLG}
\title{Miscellanées}
\date{\today}

\lhead{Langages et programmation\\\footnotesize\url{http://perso.ens-lyon.fr/bastien.le_gloannec/L1S1PCSI/}}
\rhead{L1S1 PCSI -- Université d'Orléans\\\footnotesize BLG}
\lfoot{\small\emph{Version du \today, \thistime}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\newcommand{\N}{\ensuremath{\mathbb{N}}\xspace}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}\xspace}
\newcommand{\R}{\ensuremath{\mathbb{R}}\xspace}
\newcommand{\B}{\ensuremath{\mathbb{B}}\xspace}

\newcommand{\T}{\ensuremath{\mathbb{T}}\xspace}
\newcommand{\Char}{\ensuremath{\mathbb{C}\text{har}}\xspace}

\newcommand{\vs}{\;\big\vert\;}

\newcommand{\ie}{\textsl{i.e.}\xspace}
\newcommand{\cf}{cf.\xspace}
\newcommand{\eg}{\textsl{e.g.}\xspace}
\newcommand{\ssi}{si et seulement si\xspace}
\newcommand{\ssil}{si et seulement s'il\xspace}

\newcommand{\ttf}[1]{\texttt{#1}}

\renewcommand{\labelitemi}{\textbullet}  %%% Puce enumerations

\renewcommand{\leq}{\leqslant}              %%% <= plus propre
\renewcommand{\geq}{\geqslant}              %%% >= plus propre
\newcommand{\range}[1]{\ensuremath{\{1,\ldots,#1\}}}

\newcommand{\TODO}{\textcolor{red}{\framebox{\textbf{TODO}}}\xspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{code}{\begin{ttfamily}}{\end{ttfamily}}
\newcommand{\cod}[1]{\begin{code}#1\end{code}}

\newcommand{\aff}{\ensuremath{\leftarrow}\xspace}
\newcommand{\ret}{\hspace{-1em}}

%%%%%%%%%%%%% D E B U T %%%%%%%%%%%%%%%
\begin{center}\LARGE
\textbf{Miscellanées}
\end{center}

\section{Générateur aléatoire}

\paragraph{Mode d'emploi.} Pour utiliser le générateur aléatoire, il faut :
\begin{enumerate}
\item déclarer les bibliothèques \cod{cstdlib} et \cod{ctime} au tout début du programme ;
\item initialiser le générateur aléatoire en ajoutant \framebox{\cod{srand(time(NULL));}} au début du code de la fonction \cod{main()} ;
\item appeler le générateur aléatoire en utilisant la fonction \framebox{\cod{rand()}} qui renvoie un entier aléatoire entre 0 et la constante \cod{RAND\_MAX}.
\end{enumerate}

\paragraph{Génération d'entier (type \texttt{int}).}\ 
\begin{itemize}
\item Pour un entier aléatoire entre 0 et $b>0$, on peut utiliser l'expression \framebox{\cod{rand()\%b}}.
\item Pour un entier $n$ aléatoire vérifiant $a\leq n \leq b$, on peut utiliser l'expression \framebox{\cod{rand()\%(b-a+1)+a}}.
\end{itemize}

\paragraph{Génération de réel (type \texttt{double}).}\ 
\begin{itemize}
\item Pour un réel aléatoire entre 0 et 1, on peut utiliser l'expression \framebox{\cod{(double)rand()/(double)RAND\_MAX}}.
\item Pour un réel $x$ aléatoire vérifiant $a\leq x \leq b$, on peut utiliser l'expression\\
\framebox{\cod{(double)rand()/(double)RAND\_MAX*(b-a)+a}}.
\end{itemize}

\begin{example} Le programme suivant affiche un entier aléatoire entre 1 et 1000.\normalfont
\begin{lstlisting}
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
  int n;
  srand(time(NULL));
  n = rand()%1000+1;
  cout << n << endl;
  return 0;
}
\end{lstlisting}
\end{example}

\begin{remark}
L'initialisation du générateur aléatoire avec
\framebox{\normalfont\cod{srand(time(NULL));}} utilise l'heure comme
\emph{graine} pour une méthode de génération pseudo-aléatoire. Les nombres générés
``aléatoirement'' dépendent étroitement de la graine utilisée : si
l'on fixe la graine (par exemple en oubliant simplement d'initialiser le générateur), la séquence de nombres générée est toujours la
même. Mais peut-on alors encore parler d'aléatoire ?\ldots

En pratique, fixer la graine permet de reproduire un calcul plusieurs
fois sur exactement sur la même séquence de données ``aléatoires''
(par exemple pour comparer des algorithmes ou des modélisation
physiques sur le même panel d'entrées générées aléatoirement).
\end{remark}

\newpage
\section{Manipulation des caractères (type \texttt{char})}

En C(++), les constantes de type \texttt{char} s'écrivent entre
guillemets simples \texttt{'a'}, \texttt{'b'}, \texttt{'c'}, \ldots,
\texttt{'A'}, \texttt{'B'}, \texttt{'C'}, \ldots

\paragraph{Caractères spéciaux.}\ 
\begin{itemize}
\item \texttt{'$\backslash$n'} : retour à la ligne.
\item \texttt{'$\backslash$t'} : tabulation.
\end{itemize}

\paragraph{Transtypage \texttt{char} $\longleftrightarrow$
  \texttt{int}.} Les 256 caractères du type \texttt{char} peuvent être
transtypés vers le type \texttt{int} et réciproquement afin de
réaliser facilement certaines opérations.
\begin{itemize}
\item Aux chiffres de \cod{'0'} à \cod{'9'}
 sont associés les entiers consécutifs de $\texttt{(int)'0'} = 48$ à
 $\texttt{(int)'9'} =57$.
\item Aux lettres majuscules de l'alphabet de \cod{'A'} à \cod{'Z'}
 sont associés les entiers consécutifs de $\texttt{(int)'A'} = 65$ à
 $\texttt{(int)'Z'} =90$.
\item Aux lettres de l'alphabet minuscules de \cod{'a'} à \cod{'z'}
 sont associés les entiers consécutifs de $\texttt{(int)'a'} = 97$ à $\texttt{(int)'z'} =122$.
\end{itemize}

\paragraph{Exemples d'opérations sur les caractères (type \texttt{char}).}\ 
\begin{itemize}
\item Pour vérifier qu'un caractère \cod{c} est une lettre majuscule on peut
  utiliser l'expression\\
  \framebox{\cod{(int)'A' <= (int)c \&\& (int)c <= (int)'Z'}}.
\item Pour obtenir la majuscule correspondant à une minuscule \cod{c},
  on peut utiliser l'expression\\
  \framebox{\cod{(int)c-(int)'a'+(int)'A'}}.
\end{itemize}


\section{Chaînes de caractères C++}

En C++, les chaînes de caractères sont représentées par le type
\cod{string}. On écrit une chaîne de caractères (constante) entre
guillemets doubles \cod{"Hello World!"}. En C++, contrairement au C, les chaînes de caractères sont de véritables objets et non plus simplement des tableaux de \cod{char}. Cependant elles peuvent toujours se manipuler de la même manière que des tableaux. 

\paragraph{Opérations sur les chaînes de caractères (type \texttt{string}).} Pour \cod{s} une chaîne de caractères de longueur~$l$, 
\begin{itemize}
\item \cod{s[i]} renvoie le \texttt{i}\ieme caractère de la chaîne pour $0\leq \texttt{i}\leq l-1$ ;
\item \cod{s.size()} (ou \cod{s.length()}) renvoie la longueur $l$ de \cod{s} ;
\item \cod{s1+s2} renvoie la \emph{concaténation} des deux chaînes \cod{s1} et \cod{s2} ; 
\item \cod{s.insert(i,s2)} insère la chaîne \cod{s2} en position \cod{i} dans \cod{s} ;
\item \cod{s.erase(i,n)} supprime la séquence de \cod{n} caractères démarrant en position \cod{i} dans \cod{s} ;
\item \cod{s.substr(i,n)} renvoie la séquence de \cod{n} caractères démarrant en position \cod{i} dans \cod{s}.
\end{itemize}

\iffalse
\begin{example} Le programme suivant \normalfont
\begin{lstlisting}
#include <cstdio>
#include <ctime>

int main() {
  int n;
  
  return 0;
}
\end{lstlisting}
\end{example}
\fi

\paragraph{Exercices.}
\begin{itemize}
\item Écrire un programme qui inverse (miroir) une chaîne de caractères.
\item Écrire un programme qui teste si une chaîne de caractères est un palindrome.
\item Écrire un programme qui teste si deux chaînes sont des
  anagrammes.
\item Écrire un programme qui calcule la longueur de la plus grande
  sous-chaîne uniquement constituée de la même lettre.
\item Écrire un programme qui met une chaîne en majuscules/minuscules.
\end{itemize}

%%%%%%%  F  I  N  %%%%%%%%%%%%%%%
\end{document}
