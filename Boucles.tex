\documentclass[10pt, a4paper, landscape]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tgpagella}

\usepackage{geometry}
\geometry{lmargin=1.5cm, rmargin=1.5cm, vmargin=2.5cm} %%% Marges

\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}

\usepackage{xspace}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{url}

\author{BLG}
\title{Tout ce que vous avez toujours voulu savoir sur les boucles (sans jamais oser le demander)}
\date{\today}

\lhead{Langages et programmation\\\footnotesize\url{http://perso.ens-lyon.fr/bastien.le_gloannec/L1S1PCSI/}}
\rhead{L1S1 PCSI -- Université d'Orléans\\\footnotesize BLG}
\cfoot{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\newcommand{\N}{\ensuremath{\mathbb{N}}\xspace}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}\xspace}
\newcommand{\R}{\ensuremath{\mathbb{R}}\xspace}
\newcommand{\B}{\ensuremath{\mathbb{B}}\xspace}

\newcommand{\T}{\ensuremath{\mathbb{T}}\xspace}
\newcommand{\Char}{\ensuremath{\mathbb{C}\text{har}}\xspace}

\newcommand{\vs}{\;\big\vert\;}

\newcommand{\ie}{\textsl{i.e.}\xspace}
\newcommand{\cf}{cf.\xspace}
\newcommand{\eg}{\textsl{e.g.}\xspace}
\newcommand{\ssi}{si et seulement si\xspace}
\newcommand{\ssil}{si et seulement s'il\xspace}

\newcommand{\ttf}[1]{\texttt{#1}}

\renewcommand{\labelitemi}{\textbullet}  %%% Puce enumerations

\renewcommand{\leq}{\leqslant}              %%% <= plus propre
\renewcommand{\geq}{\geqslant}              %%% >= plus propre
\newcommand{\range}[1]{\ensuremath{\{1,\ldots,#1\}}}

\newcommand{\TODO}{\textcolor{red}{\framebox{\textbf{TODO}}}\xspace}

\newcommand{\dessin}[1]{\begin{center}\framebox{\parbox{.4\textwidth}{\centering\textbf{DESSIN}\\ #1}}\end{center}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{code}{\begin{ttfamily}}{\end{ttfamily}}
\newcommand{\cod}[1]{\begin{code}#1\end{code}}

\newcommand{\aff}{\ensuremath{\leftarrow}\xspace}
\newcommand{\ret}{\hspace{-1em}}

%%%%%%%%%%%%% D E B U T %%%%%%%%%%%%%%%
\begin{center}\Large
\textbf{Tout ce que vous avez toujours voulu savoir sur les \emph{boucles} (sans jamais oser le demander)}
\end{center}
\vspace{2em}
\newcommand{\lcol}{16.3em}
\newcommand{\blocB}{\framebox{\textrm{\textit{Bloc $B$}}}}
\begin{tabular}{p{5em}|p{\lcol}|p{\lcol}|p{\lcol}|p{\lcol}|}
\centering\textbf{Boucle} & \centering\textbf{TantQue}\par (\emph{while}) & \centering\textbf{Pour}\par (\emph{for}) & \centering\textbf{Faire\ldots~TantQue}\par (\emph{do\ldots~while}) & \vbox{\centering\textbf{Répéter\ldots~Jusqu'à}\\(\emph{repeat\ldots~until})\vspace{-1.5em}}\\
\hline
\begin{center}\textbf{Syntaxe}\end{center} &\cod{TantQue ($C$) faire}\par\cod{\ \ \blocB}\par\cod{FinTantQue}\par\ \par(où $C$ est une expression booléenne)&(\cod{entier i;})\par\cod{Pour i allant de $E_d$ à $E_f$ faire}\par\cod{\ \ \blocB}\par\cod{FinPour}\par\ \par(où $E_d$ et $E_f$ sont des expressions arithmétiques)&\cod{Faire}\par\cod{\ \ \blocB}\par\cod{TantQue ($C$)}\par\ \par(où $C$ est une expression booléenne)&\cod{Répéter}\par\cod{\ \ \blocB}\par\cod{Jusqu'à ($C$)}\par\ \par(où $C$ est une expression booléenne)\\
\hline
\begin{center}\textbf{Sémantique}\end{center} & Tant que la condition représentée par l'expression booléenne $C$ est vérifiée (\ie $[C]=\text{Vrai}$), on répète l'exécution du bloc $B$.\par $C$ est (ré)évaluée \emph{avant} chaque exécution de $B$, donc si $[C]=\text{Faux}$ dès le départ, $B$ n'est jamais exécuté. & On pose les entiers $d=[E_d]$ et $f=[E_f]$ (évalués juste avant la boucle).\par Si $d\leq f$, on donne successivement à la variable \cod{i} toutes les valeurs entières de $d$ à $f$ : $d$, $d+1$, $d+2$,~\ldots{}, $f-1$, $f$ et pour chaque valeur on exécute le bloc $B$ (dans lequel on a le droit d'utiliser \cod{i}, mais pas de la modifier).\par Si $d>f$, $B$ n'est jamais exécuté. & L'exécution du bloc $B$ est répétée tant que la condition représentée par l'expression booléenne $C$ est vérifiée (\ie tant que $[C]=\text{Vrai}$). \par $C$ est (ré)évaluée \emph{après} chaque exécution de $B$, donc $B$ est toujours exécuté au moins une fois. & L'exécution du bloc $B$ est répétée jusqu'à ce que la condition représentée par l'expression booléenne $C$ soit vérifiée (\ie $B$ répété tant que $[C]=\text{Faux}$).\par $C$ est (ré)évaluée \emph{après} chaque exécution de $B$, donc $B$ est toujours exécuté au moins une fois.\\
\hline
\begin{center}\textbf{Forme équivalente}\end{center} &&(\cod{entier i;})\par\cod{i \aff $E_d$;}\par\cod{TantQue (i <= $E_f$) faire}\par\cod{\ \ \blocB}\par\cod{\ \ i \aff i + 1;}\par\cod{FinTantQue}&(\cod{booléen test;})\par\cod{test \aff Vrai;}\par\cod{TantQue (test) faire}\par\cod{\ \ \blocB}\par\cod{\ \ test \aff $C$;}\par\cod{FinTantQue}&(\cod{booléen test;})\par\cod{test \aff Faux;}\par\cod{TantQue (!test) faire}\par\cod{\ \ \blocB}\par\cod{\ \ test \aff $C$;}\par\cod{FinTantQue}\\
\hline
\begin{center}\textbf{Attention !}\end{center} & \textbf{Ne s'arrête pas toujours !}\par Il faut impérativement modifier dans $B$ les paramètres de $C$ (\ie au moins affecter une variable apparaissant dans $C$) pour que la boucle ait une chance de s'arrêter. & \textbf{S'arrête toujours}, à condition de ne surtout pas modifier dans $B$ ni la valeur de \cod{i}, ni les paramètres de $E_f$ (\ie n'affecter aucune variable apparaissant dans $E_f$). & \textbf{Ne s'arrête pas toujours !}\par Il faut impérativement modifier dans $B$ les paramètres de $C$ (\ie au moins affecter une variable apparaissant dans $C$) pour que la boucle ait une chance de s'arrêter. & \textbf{Ne s'arrête pas toujours !}\par Il faut impérativement modifier dans $B$ les paramètres de $C$ (\ie au moins affecter une variable apparaissant dans $C$) pour que la boucle ait une chance de s'arrêter.\\
\hline
\end{tabular}

%%%%%%%  F  I  N  %%%%%%%%%%%%%%%
\end{document}
