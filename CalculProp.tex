\documentclass[11pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{tgpagella}

\usepackage{geometry}
\geometry{lmargin=2.5cm, rmargin=2.5cm, vmargin=2.5cm} %%% Marges

\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}

\DeclareMathOperator{\val}{val}

\usepackage{amsthm}
\newtheorem{remark}{Remarque}
\newtheorem{example}{Exemple}

\usepackage{xspace}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{url}

\author{BLG}
\title{Logique -- Calcul propositionnel}
\date{\today}

\lhead{Langages et programmation\\\footnotesize\url{http://perso.ens-lyon.fr/bastien.le_gloannec/L1S1PCSI/}}
\rhead{L1S1 PCSI -- Université d'Orléans\\\footnotesize BLG}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\newcommand{\N}{\ensuremath{\mathbb{N}}\xspace}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}\xspace}
\newcommand{\R}{\ensuremath{\mathbb{R}}\xspace}
\newcommand{\B}{\ensuremath{\mathbb{B}}\xspace}

\newcommand{\T}{\ensuremath{\mathbb{T}}\xspace}
\newcommand{\Char}{\ensuremath{\mathbb{C}\text{har}}\xspace}

\newcommand{\vs}{\;\big\vert\;}

\newcommand{\ie}{\textsl{i.e.}\xspace}
\newcommand{\cf}{cf.\xspace}
\newcommand{\eg}{\textsl{e.g.}\xspace}
\newcommand{\ssi}{si et seulement si\xspace}
\newcommand{\ssil}{si et seulement s'il\xspace}

\newcommand{\ttf}[1]{\texttt{#1}}

\renewcommand{\labelitemi}{\textbullet}  %%% Puce enumerations

\renewcommand{\leq}{\leqslant}              %%% <= plus propre
\renewcommand{\geq}{\geqslant}              %%% >= plus propre
\newcommand{\range}[1]{\ensuremath{\{1,\ldots,#1\}}}

\newcommand{\TODO}{\textcolor{red}{\framebox{\textbf{TODO}}}\xspace}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{code}{\begin{ttfamily}}{\end{ttfamily}}
\newcommand{\cod}[1]{\begin{code}#1\end{code}}

\newcommand{\aff}{\ensuremath{\leftarrow}\xspace}
\newcommand{\ret}{\hspace{-1em}}

\newcommand{\F}{\mathcal{F}}

%%%%%%%%%%%%% D E B U T %%%%%%%%%%%%%%%
\begin{center}\LARGE
\textbf{Logique -- Calcul propositionnel}
\end{center}

L'évaluation des expressions booléennes fait intervenir les \emph{opérateurs logiques} d'une portion très restreinte de la logique mathématique : le \emph{calcul propositionnel}, que nous présentons dans ce qui suit.

\section{Introduction}

Une \emph{formule du calcul propositionnel} est un assemblage de
\emph{variables propositionnelles} (destinées à prendre une \emph{valeur de
vérité} Vrai/Faux, parfois notée 1/0) et de \emph{connecteurs
  logiques} (NON, ET, OU). Quand on affecte une valeur de vérité à
chacune des variables d'une formule (on dit que l'on choisit une
\emph{distribution de vérité}), on peut alors \emph{évaluer} cette dernière
en interprétant tous les connecteurs logique et en calculant le
résultat, la valeur ainsi obtenue est appelée \emph{valeur de vérité} de la formule.

\section{Opérateurs logiques}

On pose $\B=\{\text{Vrai},\text{Faux}\}$ l'ensemble des \emph{valeurs de vérité} (que l'on pourra aussi noter 0 pour Faux et 1 pour Vrai). On définit les opérateurs
logiques NON (unaire $\B\rightarrow\B$), noté~$\neg$, ET (binaire $\B\times\B\rightarrow\B$), noté~$\wedge$ (ou parfois~$\times$), et OU (binaire $\B\times\B\rightarrow\B$), noté~$\vee$ (ou parfois~$+$), comme des fonctions sur $\B$ vérifiant, $\forall x,y\in\B$ :
\begin{align*}
\neg x=\text{Vrai}&\overset{\Delta}{\iff} x=\text{Faux}\\
x\wedge y=\text{Vrai}&\overset{\Delta}{\iff} x=\text{Vrai}\textit{ \ \ et \ \ }y=\text{Vrai}\\
x\vee y=\text{Vrai}&\overset{\Delta}{\iff} x=\text{Vrai}\textit{ \ \ ou \ \ }y=\text{Vrai}
\end{align*}

\section{Formules du calcul propositionnel}

Les \emph{variables propositionnelles} (généralement notées $X,Y,Z\ldots$) sont des variables mathématiques pouvant prendre les valeurs Vrai ou Faux (de $\B$). On note $V$ l'ensemble de toutes les variables propositionnelles.

\paragraph{Syntaxe.} On résume la syntaxe des formules par la notation
(où $\F$ est ici un symbole représentant une formule du calcul
propositionnel) :
$$\F::= \text{Vrai}\vs \text{Faux} \vs X\in V\vs \neg \F\vs \F\wedge \F\vs \F\vee \F$$
Il faut comprendre que :
\begin{itemize}
\item Vrai est une formule ;
\item Faux est une formule ;
\item toute variable propositionnelle $X\in V$ est une formule ;
\item si $F$ est une formule, alors la \emph{négation} $\neg F$ de $F$ est
  une formule ;
\item si $F$ et $G$ sont des formules, alors la \emph{conjonction} $F\wedge
  G$ de $F$ et $G$ est une formule ;
\item si $F$ et $G$ sont des formules, alors la \emph{disjonction} $F\vee G$
  de $F$ et $G$ est une formule.
\end{itemize}

\begin{example}
$F=\neg X\vee Y$ et $G=A \wedge (B\vee\neg C)$ sont des formules du calcul propositionnel. Pour mettre en valeur les variables dont dépendent ces formules, on peut aussi noter de façon fonctionnelle $F(X,Y)$ et $G(A,B,C)$ au lieu de $F$ et $G$.
\end{example}

\begin{remark} On ne met pas ici les \emph{parenthèses} dans la grammaire, elle seront
utilisées pour indiquer la \emph{structure} (essentiellement la
priorité d'une opération sur une autre) d'une formule.
\end{remark}

\paragraph{Distribution de vérité.} Une \emph{distribution de vérité} $\delta$ est l'attribution d'une valeur de vérité (Vrai ou Faux) à chaque variable propositionnelle de $V$, c'est donc une fonction $\delta : V\rightarrow\B$. On note $\B^V$ l'ensemble des distributions de vérité.

\paragraph{Interprétation.} Étant données une formule $F$ et une distribution de vérité $\delta\in\B^V$, on \emph{interprète} $F$ en remplaçant chacune des variables propositionnelles $X$ apparaissant dans $F$ par sa valeur de vérité $\delta(X)$ et en effectuant le calcul logique représenté par la formule. Le résultat de ce calcul est une valeur de vérité notée $\val_{\delta}(F)$.

\begin{remark} En l'absence de parenthèses pour indiquer le contraire, l'opérateur de
négation $\neg$ sera considéré prioritaire sur les opérateurs $\wedge$ et $\vee$ dans l'évaluation d'une formule.
\end{remark}

\begin{example}
On reprend $F=\neg X\vee Y$ et $G=A \wedge (B\vee\neg C)$. Si $\delta$ est une distribution telle que $\delta(X)=\delta(A)=\delta(B)=\text{Vrai}$ et $\delta(Y)=\delta(C)=\text{Faux}$, alors :

$\val_{\delta}(F)=\neg\delta(X)\vee\delta(Y)=\neg\text{Vrai}\vee\text{Faux}=\text{Faux}\vee\text{Faux}=\text{Faux}$ ;

$\val_{\delta}(G)=\delta(A)\wedge (\delta(B)\vee\neg \delta(C))=1\wedge (1\vee\neg 0)=1\wedge1=1$.
\end{example}

\paragraph{Distributions partielles et table de vérité.} La valeur de vérité d'une formule $F$ dans une distribution $\delta$ ne dépend que des valeurs affectées par la distribution aux variables propositionnelles (en nombre fini) apparaissant dans $F$. Si on note $V_F$, cet ensemble on peut se contenter pour étudier $F$ de considérer les distributions \emph{partielles} de vérité $\delta_F : V_F\rightarrow\B$ qui sont en nombre fini.

La \emph{table de vérité} d'une formule $F$ est la représentation dans un tableau (avec les variables puis $F$ sur les colonnes et les différentes distributions sur les lignes) de toutes les distributions partielles de vérité $\delta_F$ possibles \emph{pour les variables apparaissant dans $F$} et des valeurs $\val_{\delta_F}(F)$ correspondantes.

\begin{example} On reprend $F=\neg X\vee Y$ et $G=A \wedge (B\vee\neg C)$.
\begin{center}
\begin{tabular}{cc|c}
$X$ & $Y$ & $F$\\
\hline
0&0&1\\
0&1&1\\
1&0&0\\
1&1&1
\end{tabular}
\hspace{5em}\small
\begin{tabular}{ccc|c|c}
$A$ & $B$ & $C$ & $B\vee\neg C$ & $G$\\
\hline
0&0&0&1&0\\
0&0&1&0&0\\
0&1&0&1&0\\
0&1&1&1&0\\
1&0&0&1&1\\
1&0&1&0&0\\
1&1&0&1&1\\
1&1&1&1&1
\end{tabular}
\end{center}
\end{example}

\paragraph{Équivalence logique $\equiv$.} Deux formules $F$ et $G$ sont \emph{logiquement équivalentes}, ce que l'on note $F\equiv G$, \ssi elles ont la même valeur de vérité pour toute distribution de vérité, \ie $\forall \delta\in\B^V$, $\val_{\delta}(F)=\val_{\delta}(G)$.

Une formule $F$ est une \emph{tautologie} si $F\equiv\text{Vrai}$, \ie elle est vraie pour toute distribution de vérité (\ie quelles que soient les valeurs prises par ses variables).

\begin{example}
$X\vee\neg X$ est une tautologie.
\end{example}

\begin{remark} On sait décider si une formule du calcul propositionnel est une tautologie : il suffit de l'évaluer sur toutes les distributions (partielles) de vérité (par exemple en construisant sa table de vérité).

On sait aussi décider si deux formules sont équivalentes, il suffit de vérifier qu'elles ont la même table de vérité.
\end{remark}

%%%%%%%  F  I  N  %%%%%%%%%%%%%%%
\end{document}
